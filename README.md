# Nemesis

Nemesis is a Python library for dealing with Machine Learning.

## Installation

Use the package manager to install nemesis.

```bash
pip install nemesis
```

## Usage

```python
import nemesis

nemesis.loaddata(data.csv) # loads csv file to memory
foobar.predict('price') # runs prediction and returns prediction

## NOTE

For License I Hvae used Apache License 2.0 - A permissive license whose main conditions require preservation of copyright and license notices. Contributors provide an express grant of patent rights. Licensed works, modifications, and larger works may be distributed under different terms and without source code. This seems to suit my Capstone project ideally as client would want to license the work and acquire patent rights for their intellecual or creative property. They would also want their work distributed without ever revealing the source code.

-------- VANEESH JHA ---------------
